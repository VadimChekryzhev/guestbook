<?php session_start(); ?>
<?php require_once 'config.php' ?>

<!doctype html>
<html lang="ru">
<head>
  <meta charset="UTF-8">
  <meta name="viewport"
        content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <link rel="stylesheet" href="css/style.css">
  <title>Document</title>
</head>
<body>

<!--<script src="https://www.google.com/recaptcha/api.js"></script>-->


<h1>Гостевая книга группы 574M</h1>
<div class="wrapper">
  <div class="sender">
    <form method="" class="form">
      <caption><span class="caption">Логин:</span></caption>
      <label for="login_f"></label><br>
      <input type="text" id="login_f" name="login" placeholder="фамилия.группа" required><br>
      <label for="message">Текст сообщения: </label><br>
      <textarea name="message" id="message" cols="40" rows="10" required></textarea>
      <!--      <div id="recaptcha">-->
      <!--<!--        <div class="g-recaptcha" data-sitekey="--><? ////= PUBLIC_RECAPCHA ?><!--<!--"></div>-->
      <!--      </div>-->
      <button id="send">Отправить</button>
    </form>
    <div id="answer"></div>
  </div>

  <div id="messages"></div>


</div>

<script src="js/jquery.min.js"></script>
<script src="js/script.js"></script>

</body>
</html>