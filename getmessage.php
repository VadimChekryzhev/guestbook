<?php
session_start();
require_once 'db.php';

//  $stmt = $dbh->query('SELECT * FROM message')->fetchAll();

if (!$_POST['col_mess']) {
  $stmt = $dbh->query('SELECT * FROM message')->fetchAll();
  $count = count($stmt);
  $_SESSION['count'] = $count;
} else {
  $stmt = $dbh->query("SELECT COUNT(*) FROM `message`")->fetchAll();
  $new = +($stmt[0][0]) - $_SESSION['count'];
  if (!$new) {
    $stmt = '';
  } else {
    $stmt = $dbh->query("SELECT * FROM message ORDER BY id DESC LIMIT $new")->fetchAll();
    $_SESSION['count'] += $new;
  }
}

session_write_close();

$stmt = json_encode($stmt);

echo $stmt;