<?php session_start(); ?>
<?php require_once 'config.php' ?>

<!doctype html>
<html lang="ru">
<head>
  <meta charset="UTF-8">
  <meta name="viewport"
        content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <link rel="stylesheet" href="css/style.css">
  <title>Document</title>
</head>
<body>

<!--<script src="https://www.google.com/recaptcha/api.js"></script>-->

<!--  <h1>Добро пожаловать в гостевую книгу 574М</h1>-->
  <div class="buttons">
    <a href="#" class="button">Войти</a>
    <a href="#" class="button">Регистрация</a>
  </div>



<script src="js/jquery.min.js"></script>
<script src="js/script.js"></script>


<!-- HelloPreload http://hello-site.ru/preloader/ -->
<style type="text/css">#hellopreloader > p {
    display: none;
  }
  #hellopreloader_preload {
    display: block;
    position: fixed;
    z-index: 99999;
    top: 0;
    left: 0;
    width: 100%;
    height: 100%;
    min-width: 420px;
    background: #2C3E50 url(http://hello-site.ru//main/images/preloads/rings.svg) center center no-repeat;
    background-size: 125px;
  }</style>
<div id="hellopreloader">
  <div id="hellopreloader_preload"></div>
<script type="text/javascript">var hellopreloader = document.getElementById("hellopreloader_preload");

  function fadeOutnojquery(el) {
    el.style.opacity = 1;
    var interhellopreloader = setInterval(function () {
      el.style.opacity = el.style.opacity - 0.05;
      if (el.style.opacity <= 0.05) {
        clearInterval(interhellopreloader);
        hellopreloader.style.display = "none";
      }
    }, 16);
  }

  window.onload = function () {
    setTimeout(function () {
      fadeOutnojquery(hellopreloader);
    }, 1000);
  };</script>
<!-- HelloPreload http://hello-site.ru/preloader/ -->
</body>
</html>