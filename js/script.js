$(document).ready(function () {

  function getMessage() {
    var messages = $('#messages');
    var col_mess = $('.mess').length;

    $.ajax({
      url: 'getmessage.php',
      type: 'POST',
      data: ({'col_mess': col_mess}),
      dateType: 'html',
      success: function (data) {
        var all_messages = JSON.parse(data);
        var i;
        for (i = 0; i < all_messages.length; i++) {
          messages.prepend('<div class="mess">\n' +
            '  <p class="title"><span class="autor">' + all_messages[i]['user'] + '</span>\n' +
            '    <span class="date">' + all_messages[i]['date'] + '</span></p>\n' +
            '  <p>' + all_messages[i]['message'] + '</p>\n' +
            '</div>');
        }
      },
      // error: function () {
      //   messages.prepend('<div class="mess error">\n' +
      //     '  <p class="title"><span class="autor">admin</span>\n' +
      //     '    <span class="date">2017-09-23 17:02</span></p>\n' +
      //     '  <p>Сервер не отвечает. Приносим извенения за неудобства.</p>\n' +
      //     '</div>');
      // }
    });
  }

  setInterval(getMessage, 2000);

  $('#send').click(function (event) {
    event.preventDefault();
    // var form = $('.form');
    var answer = $('#answer');
    var user = $('#login_f').val();
    var message = $('#message').val();

    $.ajax({
      url: 'send.php',
      type: 'POST',
      // data: form.serialize(),
      data: ({'user': user, 'message': message}),
      dataType: "html",
      beforeSend: function () {
        answer.text("Отправка данных...");
      },
      success: function (data) {
        answer.text(data);
        $('#message').val('');
      },
      error: function () {
        answer.text('Ошика соединения (лотерея)');
      }
    });
  });

});