<?php

require_once 'db.php';
require_once 'function.php';

$group = require 'login.php';

//if (!$_POST['g-recaptcha-response']) {
//  echo('Капча введена неверно 1');
//  exit();
//}
//
//$url = 'https://www.google.com/recaptcha/api/siteverify?secret='.SECRET_RECAPCHA.'&response='.$_POST['g-recaptcha-response'].'&remoteip='.$_SERVER['REMOTE_ADDR'];
//$data = json_decode(file_get_contents($url));
//if ($data->success == false){
//  echo('Капча введена неверно 2');
//  exit();
//}

$_POST = defender_xss($_POST);

$user = mb_strtolower(trim($_POST['user']));
$message = trim($_POST['message']);
$date = date("Y-m-d H:i:s");

if (!$user) {
  echo "Твой логин пуст!";
  exit();
}

if(!$message) {
  echo "Где твоё сообщение?";
  exit();
}

if (in_array($user, $group)) {
  $sql = "INSERT INTO message (user, message, date) VALUE(:user, :group, :date)";
  $sth = $dbh->prepare($sql);
  $sth->bindParam(':user', $user);
  $sth->bindParam(':group', $message);
  $sth->bindParam(':date', date("Y-m-d H:i:s"));
  $sth->execute();
  echo 'Ваше сообщение отправлено!';
} else {
//  echo $_POST['login_f'];
  echo 'В 574М нет такого студента';
}